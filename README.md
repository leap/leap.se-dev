# leap.se website

## Install

1. Clone the repo:

  git clone https://0xacab.org/leap/leap.se
  cd leap-se-dev

2. Initialize the theme:

  git submodule update --init --recursive

3. Deployment from 0xacab should happen from main branch


## Run 

Add your content, and run a local server:

  hugo server


## Images

Place your image as `featured.png` in the data folder for the post.
