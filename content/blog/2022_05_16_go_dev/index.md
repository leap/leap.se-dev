---
title: "Wanted: Go Application Developer to Fight Censorship"
slug: "wanted-go-app-developer-to-fight-censorship"
summary: "Do you know a Go app developer interested in anti-censorship?"
authors:
- leap
tags:
- job
categories:
- vpn
- dev
- go
date: "2022-05-14T00:00:00Z"
lastmod: "2022-05-15T00:00:00Z"
featured: true
draft: false
---

## Wanted: Go Application Developer to Fight Censorship

If you're a Go developer who is passionate about internet freedom and privacy and are interested in VPNs and circumvention technology come work with us! The LEAP Encryption Access Project (LEAP) is looking for an application developer to join the team behind LEAP VPN: a multiplatform VPN application with a focus on usability and censorship-circumvention.  

### Job description

If you join the team, you will be part of the development of LEAP VPN, a white-label  Free Software VPN application based on OpenVPN that is currently deployed by providers like Riseup, Calyx and CodigoSur. Your work will focus on the development and integration of Pluggable Transports to enable better censorship circumvention as well as maintaining the Desktop LEAP VPN application. This is a remote part-time contract position, starting ASAP.

- Time: Part time
- Where: Remote
- Start Date: ASAP
- Application Deadline: May 30th, 2022.
- Pay: Commensurate with experience

### Required qualifications

- You must be a self-directed person, able to work efficiently under light management, and work well within a distributed team. Good communication skills are essential.
- Knowledge of the Go programming language.
- Good testing and debugging skills.
- Ability to quickly prototype and deploy network services, perform measurements and iterate as part of a collaborative design process.
- Familiarity with git and GitLab.
- Experience with Free Software/Open Source.
- Be comfortable working remotely with a geographically distributed team.

### Preferred qualifications

- Strong experience with Qt5 and QML.
- Familiarity with OpenVPN and censorship circumvention technologies.
- Experience with Docker and Ansible.


### LEAP's Crew
We've been building tools for secure communication for over a decade. Our team is kind, supportive,and passionate about the work we do. Our streamlined agile development process is light on management and heavy on collaboration. Our team is fully distributed with staff in Germany, Spain, Canada, and USA. We look forward to meet you!

### How to Apply
To apply, please email us the following documents to info (at) leap (dot) se, before May 31st, 2022. Please avoid pdf: use any plaintext format at your convenience (md, latex, .c, .go, etc).

- Cover letter. Let us know how your qualificaitons and experience meet the requirements of this job position. Please include the reasons why you want to work with us. Include a link to your public GitHub/GitLab profile 
- CV/resume
- Make sure to include at least 3 code samples

We especially welcome applicants from communities underrepresented in the tech sector.


