---
title: We're hiring
slug: we-are-hiring-2021
summary: we've got work to do, and we're welcoming applications
subtitle: and we'd like you to apply!
authors:
- leap
tags:
- jobs
- leap
categories:
- vpn
date: "2021-11-28T00:00:00Z"
lastmod: "2021-11-28T00:00:00Z"
featured: true
draft: false
---

LEAP builds VPNs used by [Riseup](https://riseup.net/), [The Calyx Institute](https://calyxinstitute.org/), US public libraries, and others. Starting in 2022 we will be helping to build TorVPN. We are looking for a Go application developer, Android developer, and an iOS developer to join our team. Come build tunnels with us! 

We're looking for:

1) [A go developer](#go-application-developer-for-leap-vpn).
2) [An android developer](#android-app-developer-for-leap-vpn).
3) [An iOS developer](#ios-developer-for-leap-vpn).

See below for information on each of the positions, and at the end of this post for instructions on [how to apply](#how-to-apply).

---

## Go Application developer for LEAP VPN

The LEAP Encryption Access Project (LEAP) is looking for an application
developer to join the team behind LEAP VPN: a multiplatform VPN application
with a focus on usability and censorship-circumvention. The ideal candidate is
a skilled Go developer who is passionate about internet freedom and privacy.
Please send your application before December 31st, 2021.

* **Time:** Part time
* **Start Date:** Beginning of January
* **Pay:** \$40/hr

### Job description

If you join the team, you will be part of the development of LEAP VPN,
a white-label  Free Software VPN application based on OpenVPN that is currently
deployed by providers like Riseup, Calyx and CodigoSur.
Specifically, your work will focus on maintaining the Desktop LEAP VPN application.
You will also participate in the development and integration of Pluggable
Transports, to enable better censorship circumvention.

This is a part-time contract position, starting January 2022. This is a remote
position: our team is fully distributed with staff in Germany, Spain, Canada,
and USA.

### Required qualifications

- You must be a self-directed person, able to work efficiently under light management, and work well within a distributed team. Good communication skills are essential.
- Knowledge of the Go programming language.
- Good testing and debugging skills.
- Ability to quickly prototype and deploy network services, perform measurements and iterate as part of a collaborative design process.
- Familiarity with git and GitLab.
- Experience with Free Software/Open Source.
- Be comfortable working remotely with a geographically distributed team.

### Preferred qualifications

- Strong experience with Qt5 and QML.
- Familiarity with OpenVPN and censorship circumvention technologies.
- Experience with Docker and Ansible.

---

## Android App developer for LEAP VPN

The LEAP Encryption Access Project (LEAP) is looking for a mobile application
developer to join the team behind LEAP VPN: a multi-platform VPN application
with a focus on usability and censorship-circumvention. Work will include
development of LEAP VPN, Tor VPN, and Pluggable Transports integration. The
ideal candidate is highly self motivated, has experience with Android VPN
development and is passionate about internet freedom and privacy. Knowledge of
the programming language Rust is a plus.

Please send your application before December 31st, 2021.  

* **Time:** Part time  
* **Start Date:** Beginning of January
* **Pay:** \$40/hr  
  
### Job description

If you join the team, you will be part of the development of LEAP VPN,
a white-label  Free Software VPN application based on OpenVPN that is currently
deployed by providers like Riseup, Calyx and CodigoSur. You will also work with
LEAP developers and Tor developers on the new Tor VPN.

You should have an in-depth understanding of the Android SDK and ideally have
experience with VPN API Android offers. You should be familiar with writing
testable code, unit tests and instrumentation tests to guarantee code quality.
Your work on the new Tor VPN makes having Rust experience a big plus.

You have experience with golang? Great! We're using circumvention tech in our VPN clients written in golang that needs to keep being updated as well.

This is a part-time position, starting January 2022.

This is a remote position: our team is fully distributed with staff in Germany, Spain, Canada, and USA.  

### Required qualifications

- You must be a self-directed person, able to work efficiently under light
  management, and work well within a distributed team. Good communication
  skills in english language are essential.
- Knowledge of Android development with Java and Kotlin
- Familiarity with censorship circumvention technologies, like VPNs, Tor and Pluggable Transports
- Good testing and debugging skills.
- Familiarity with git and GitLab.
- Experience with Open Source software development
- Be comfortable working remotely with a geographically distributed team.  

### Preferred qualifications

- Knowledge of Rust
- Knowledge of golang
- Familiarity with OpenVPN
- Familiarity with snowflake and obfs4
- Experience with Docker and Ansible.
- Ability to quickly prototype and deploy network services, perform
  measurements and iterate as part of a collaborative design process.  


---

## iOS developer for LEAP VPN

The LEAP Encryption Access Project (LEAP) is looking for an mobile application
developer to join the team behind LEAP VPN: a multiplatform VPN application
with a focus on usability and censorship-circumvention. The ideal candidate is
a person with knowlege of iOS development and the Go programming language who
is passionate about internet freedom and privacy. Please send your application
before December 31st, 2021.  

* **Time:** Part time  
* **Start Date:** At some point in 2022
* **Pay:** \$40/hr  

### Job description

If you join the team, you will be part of the development of LEAP VPN,
a white-label  Free Software VPN application based on OpenVPN that is currently
deployed by providers like Riseup, Calyx and CodigoSur.

Your work will mainly focus on developing an initial prototype of an iOS VPN
client reusing and enhancing our bitmask core library written in go. You will
also participate in the development and integration of Pluggable Transports, to
enable better censorship circumvention.

This is a part-time contract position, starting first quarter of  2022. 

This is a remote position: our team is fully distributed with staff in Germany, Spain, Canada, and USA.

### Required qualifications

- Knowledge of iOS development with Swift.
- Knowledge of the Go programming language.
- Good testing and debugging skills.
- Familiarity with git and GitLab.
- Experience with Free Software/Open Source.
- Be comfortable working remotely with a geographically distributed team.
- You must be a self-directed person, able to work efficiently under light
  management, and work well within a distributed team. Good communication
  skills are essential.

### Preferred qualifications  

- Familiarity with OpenVPN and censorship circumvention and/or measurement technologies.
- Knowledge in Android development is a plus

---

## Some Background

LEAP VPN is a white label, Free Software VPN system in use on Android, Windows,
Mac, Linux and OpenWRT-based routers. LEAP VPN is branded for our trusted
partners Riseup and The Calyx Institute, and is the foundation of the Bitmask,
RiseupVPN, CalyxVPN, and LibraryVPN applications. We use OONI Probe to monitor
blockage attempts, and we use Pluggable Transports (obfs4 and snowflake) to
evade censorship. Our UI is translated into over 30 languages.

## How to apply

To apply, please email us the following documents to info (at) leap (dot) se, before December 31st, 2021:

- CV/resume. Make sure to include at least 3 code samples, and a link to your
  public GitHub/GitLab profile. Please avoid pdf: use any plaintext format at
  your convenience (md, latex, .c, .go, etc).
- Cover letter. Let us know how your "koalafications" and experience meet the
  requirements of this job position. Please include the reasons why you want to
  work with us.
- Make sure to include in the subject the positon(s) you're applying to.

We especially welcome applicants from communities underrepresented in the tech sector.
