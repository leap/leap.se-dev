---
title: New release!
slug: announcing-ux-love-release
summary: a new release is out, and it brings eye-candy
subtitle: Introducing our latest LEAP VPN "UX 🖤" release, and why we're so excited about it
authors:
- leap
tags:
- releases
categories:
- vpn
date: "2021-12-23T00:00:00Z"
lastmod: "2021-12-23T00:00:00Z"
featured: true
draft: false
---

We are excited to announce a new release of LEAP VPN. With help from [Simple
Secure](https://simplysecure.org/) we’ve embraced a user centered development
methodology, conducting multiple rounds of prototyping and field testing to
create a clean, responsive UX. This release is available on Linux, Mac, Windows,
and Android systems and will be available on iOS in 2022. LEAP VPN is the
shared code base used by RiseupVPN, CalyxVPN, CodigoSur, Colnodo and Bitmask.
Users of these services will get the new UX when their providers update to the
latest version of the LEAP VPN clients.

The new UX is [already available for RiseupVPN users](https://riseup.net/vpn) from the usual locations.

![Screenshots for the new ui on the desktop app, on three different states](ux-revamp-screenshots.jpg "Who doesn't love some privacy birds to cheer up their desktop?")

## New Features

* A fully redesigned user interface, including some lightweight theming
  mechanism for the different providers.
* Better gateway selection
* Faster connection using UDP 
* Message of the day! Providers can now alert you through the application of
  important news, like when a new release is available.


## Coming Soon

We have been working to improve the ability to use LEAP VPN where VPN is
blocked. In the coming weeks we will have a new release that integrates two
[Pluggable Transports](https://www.pluggabletransports.info/):
[obfs4](https://support.torproject.org/glossary/obfs4/), and
[Snowflake](https://snowflake.torproject.org/) - technology that helps
bypassing censorship. And of course, we'll keep on improving the UX based on
the feedback of our providers and you all.
