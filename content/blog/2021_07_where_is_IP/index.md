---
title: On IPs and locations
slug: on-ips-and-locations
summary: we try to answer the basic questions about locations, IPs and legal consequence
subtitle: IPs are everywhere, and yet they're not where we think they are
authors:
- kwadronaut
tags:
- faq
- GeoIP
categories:
- vpn
date: "2021-07-12T00:00:00Z"
featured: true
draft: false
---

## Where do the troubles think I am? And where is my provider? Where are the troubles? 

A VPN is rather about juggling than magic. You do not disappear, you hide. Whatever you do, some people will try to see your shadow. Let us try to shed some light on the often misunderstood "IP" and "location". 

## "I connect to this IP but when checking, the internet tells me I have another IP?" 

With our VPN architecture, there are several IPs involved. First: there is your personal one, the one you do not want to be out in the public. That one connects securely to a gateway (second IP) and leaves over a third IP address to the internet. It is this third IP that will be seen by outsiders. Let's call that the exit or "egress" IP. [This paper](https://www.usenix.org/system/files/conference/foci12/foci12-final8.pdf) explains why.

## "This site says I'm located in Kansas, not New York!" 

That site, like many others, rely on so called GeoIP databases. They look up the exit IP of the VPN in those databases. Quite often, they are not very precise nor up to date. Sadly, that is something we can't change. On top of that, it usually implies that you will get to see more captchas or get locked out of the most interesting content. The curious reader asking about Kansas can [read the local news](https://www.kansas.com/news/local/article94447622.html).

## "I want to connect to that city! 

Most of the time you will be connected to the nearest gateway that is not overloaded. You can use the gateway selector to choose another one. When the GeoIP database of the service you wish to use has the correct data, things will be as you wish. If however we don't have any exit points in that city, donate donate donate and tell us about good providers around that location.

## "Jurisdiction?" 

The providers that are operating CalyxVPN or RiseupVPN (or another one) are the ones that receive the legal requests. Wherever they are registered is relevant for whatever legalities. Luckily, I'm just a simple writer of this post and don't know anything more than that.
